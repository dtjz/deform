<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>  
<meta charset="utf-8">
  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta name="keywords" content="表单列表">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/easyui/1.5.3/themes/metro/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/easyui/1.5.3/themes/icon.css"> 

 <title>工单模板列表</title>

  <style>
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
	font: small/1.5 "宋体", serif;
	overflow-y: hidden;
}
 
</style>

</head>
<body>
	<div id="right_top" class="right_top" >
			 
			<div style="width: 100%;padding: 10px; ">
			   
				<a href="#" class="easyui-linkbutton" id="add_form" data-options="iconCls:'icon-add'" style="width:80px">添加</a>
        		<a href="#" class="easyui-linkbutton" id="remove_form" data-options="iconCls:'icon-remove'" style="width:80px">删除</a>
        		
			</div>
			     
			
		</div>
	<hr align="center" width="100%"  size="2px">  
			<table id="dg_stat" class="easyui-datagrid" style="width:100%;height:auto;" data-options="rownumbers:'true',checkbox:true,singleSelect:false,url:'./formList'">
				<thead>
					<tr> 
						 <th data-options="field:'ck',checkbox:true"></th>
						<th field="form_name" width="150">表单名称</th>
						<th data-options="field:'crtime', width:'200',align:'center',formatter:function(v){
							var d = new Date(v);
							return d.Format('yyyy-MM-dd hh:mm:ss');
						}" >创建时间</th> 
						 
						<th data-options="field:'modify_time', width:'200',align:'center',formatter:function(v){
							var d = new Date(v);
							return d.Format('yyyy-MM-dd hh:mm:ss');
						}" >最后修改时间</th> 
						<th data-options="field:'a', width:'150',align:'center',formatter:function(v,n){
                			var str =  '<a  href=\'#\' class=\'easyui-linkbutton\'  onclick=preview(' + n.form_id + ')>预览</a>' ;
                			str += '&nbsp;<a  href=\'#\' class=\'easyui-linkbutton\'  onclick=modify(' + n.form_id + ')>修改 </a>' ;
                			str += '&nbsp;<a  href=\'#\' class=\'easyui-linkbutton\'  onclick=go(' + n.form_id + ')>发起工单 </a>' ;
                			return str ;
                		}" >操作</th> 
					</tr>
				</thead> 
            </table> 
		 
   
   <!-- form -->	  
   	  
  <div id="form_edit"   class="easyui-dialog" title="动态表单绘制" data-options="closed:true,maximizable:true,maximized:true,resizable:true,modal:true" style="width:1000px;height:500px;padding:10px;display: none;overflow: hidden;">
	 
	<iframe id="frame_form_edit" src="./edit" style="height: 100%;width: 100%"></iframe>

  </div>
  
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/easyui/1.5.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/easyui/1.5.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/lib/public.js"></script> 
 
<%-- 
 <script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/ueditor.config.js?v=2023"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/ueditor.all.js?v=2023"> </script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/lang/zh-cn/zh-cn.js?v=2023"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/res/js/ueditor/formdesign/leipi.formdesign.v4.js?v=2023"></script>
<!-- script start-->  
<script type="text/javascript" src="${pageContext.request.contextPath}/res/view/form_edit.js"> </script>
 --%>

<script type="text/javascript">
   	$('#add_form').bind('click', function(){
	       // alert('search');
	       console.log('##############');
	       document.getElementById('frame_form_edit').src = "./edit";
   		$('#form_edit').dialog('open');
	        
	 });
   	
	$('#remove_form').bind('click', function(){
		var select = $('#dg_stat').datagrid('getChecked');
    	if( !select  ) { 
			return ;
		}
    	
    	var ids = [];
    	for(var i in select) {
    		ids.push(select[i].form_id);
    	}
    	console.log("ids : " + ids );
   	 $.messager.confirm('删除确认', '您确定要删除(删除后不可恢复)?', function(r){
   		 if (r){
   			 $.ajax({  
   	 	         url: './removeForm' ,  
   	 	         type: 'POST',  
   	 	         data: {forms : ids},   
   	 	         success: function (ret) {   
   	 	        	 if(ret.message) {
   	 	        		 $.messager.alert('提示',ret.message,'warning');
   	 	        	 }else if(ret.status == 0) {
   	 	        		 
   	 	        		$('#dg_stat').datagrid('reload');
   	 	        	 }
   	 	       	  	$.messager.progress('close');
   	 	         },  
   	 	         error: function (returndata) {  
   	 	            // alert('fail:' + returndata);   
   	 	             $.messager.progress('close');
   	 	         } 
   	 	    });  
   		 }
   	 });
	        
	 });
   	
   	function closeEdit(){
   		$('#form_edit').dialog('close');
   		
   	}
   	
   	function go(form_id) {
   		window.open( './start_edit?form_id=' + form_id , '工单填写');
   		
   		
   	}
   	
   	function preview(form_id) {
   		
   		window.open( './previewEditForm?form_id=' + form_id +'&type=1' , '自定义表单预览');
   		
   	}
   	
   	function modify(form_id) {
   		
   		document.getElementById('frame_form_edit').src = "./edit?form_id=" + form_id ;
   		
   		$('#form_edit').dialog('open');
   	}
   	 	
   	 	 
</script>
</body>
</html>