package deform;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchesJsp {

	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		
		String regex = "(?i).*<%[^<]*(7jyewu|java\\.io|java\\.util|java\\.net|runtime|JFolder|webshell)[^>]*%>.*" ;
		
		Pattern p = Pattern.compile(regex);
		
		File dir = new File("e:/jsp/");
		File[] fs = dir.listFiles();
		for(File f : fs) {
			String context = readFileContext(f);
			if(context == null) continue ;
			//System.out.println(context);
			Matcher m = p.matcher(context);


			System.out.println(f.getName() + " : " + m.matches() );
				 
		}
		
		long end = System.currentTimeMillis();
		
		System.out.println("use time : " + (end - start) + " ms");
	}
	
	
	public static String readFileContext(File f) {
		
		if(!f.exists()) return null ;
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder(10 * 1024);
		try {
			br = new BufferedReader(new FileReader(f)) ;
			String line = null;
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			
			return sb.toString();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
		
		
		
	}

}
